#!/bin/bash

RDS_INSTANCE=interview.caczpaa0lem1.us-west-2.rds.amazonaws.com
DB_USERNAME=interview
DB_PASSWORD=password
DB_NAME=interview

to_archive=$(while read table; do echo "$table"; done < roles/table_archive/files/archive_tables.txt)
cslist=$(echo $to_archive | tr ' ' ,)
mysql -h $RDS_INSTANCE -u $DB_USERNAME -p$DB_PASSWORD $DB_NAME -e "drop table if exists $cslist";